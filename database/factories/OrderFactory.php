<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Client;
use App\Models\Product;

class OrderFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_order' => $this->faker->dateTime(),
            'client_id' => function () {
                return Client::all()->random()->id;
            },
            'products' => function () {
                $products = array();
                for ($i = 0; $i <= $this->faker->numberBetween(1, 15); $i++) {
                    $product = Product::all()->random();
                    $products[$i]['id'] = $product->id;
                    $products[$i]['price'] = $product->price;
                    $products[$i]['quantity'] = $this->faker->numberBetween(1, 15);
                    $products[$i]['subtotal'] = $products[$i]['quantity'] * $product->price;
                }
                return collect($products)->toJson();
            },
        ];
    }

}
