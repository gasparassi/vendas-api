<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cep' => $this->faker->postcode,
            'logradouro' => $this->faker->streetName,
            'numero' => $this->faker->buildingNumber,
            'bairro' => $this->faker->state,
            'localidade' => $this->faker->city,
            'uf' => $this->faker->regionAbbr,
            'complemento' => $this->faker->realText(45),
            'ibge' => $this->faker->numberBetween(0, 9999999),
            'ddd' => $this->faker->numberBetween(0, 100),
        ];
    }
}
