<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::get('/', function () {
    return response()
                    ->json([
                        'message' => 'Cadastro de Clientes API',
                        'status' => 'Connected',
                        'statusCode' => 200,
                            ],
                            200);
}
);

Route::group([
    'prefix' => 'v1',
        ], function () {
    Route::group([
        'prefix' => 'clients',
            ], function () {
        Route::post('/', [ ClientController::class, 'store' ]);
        Route::get('/', [ ClientController::class, 'index' ]);
        Route::get('/{id}', [ ClientController::class, 'show' ]);
        Route::delete('/{id}', [ ClientController::class, 'destroy' ]);
        Route::put('/{id}', [ ClientController::class, 'update' ]);
    });
    Route::group([
        'prefix' => 'addresses',
            ], function () {
        Route::get('/{cep}', [ AddressController::class, 'requestFullAddressFromExternApi' ]);
    });
    Route::group([
        'prefix' => 'products',
            ], function () {
        Route::post('/', [ ProductController::class, 'store' ]);
        Route::get('/', [ ProductController::class, 'index' ]);
        Route::get('/{id}', [ ProductController::class, 'show' ]);
        Route::delete('/{id}', [ ProductController::class, 'destroy' ]);
        Route::put('/{id}', [ ProductController::class, 'update' ]);
    });
    Route::group([
        'prefix' => 'orders',
            ], function () {
        Route::post('/', [ OrderController::class, 'store' ]);
        Route::get('/', [ OrderController::class, 'index' ]);
        Route::get('/{id}', [ OrderController::class, 'show' ]);
        Route::delete('/{id}', [ OrderController::class, 'destroy' ]);
        Route::put('/{id}', [ OrderController::class, 'update' ]);
    });
});
