<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $productsArray = json_decode($this->products);
        
        $total = 0;
        for ($i = 0; $i < count($productsArray); $i++) {
            $total += $productsArray[$i]->subtotal;
        }
        
        return [
            'id' => $this->id,
            'date_order' => $this->date_order,
            'client_id' => $this->client_id,
            'products' => $this->products,
            'total' => $total,
        ];
    }

}
