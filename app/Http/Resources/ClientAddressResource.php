<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'logradouro' => $this->logradouro,            
            'complemento' => $this->complemento,                    
            'bairro' => $this->bairro,
            'localidade' => $this->localidade,
            'uf' => $this->uf,            
            'cep' => $this->cep,
            'ddd' => $this->ddd,
            'ibge' => $this->ibge, 
        ];
    }
}
