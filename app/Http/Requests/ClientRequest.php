<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;

class ClientRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|string|max:100',
            'logradouro' => 'required|string|max:100',
            'numero' => 'required|integer',
            'bairro' => 'required|string|max:50',
            'localidade' => 'required|string|max:50',
            'uf' => 'required|string|max:2',
            'cep' => 'required|string|max:9',
            'complemento' => 'string|max:40',
            'ibge' => 'required|integer',
            'ddd' => 'required|integer',
        ];
    }

}
