<?php

namespace App\Http\Controllers;

use App\Services\OrderService;
use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderResource;

class OrderController extends Controller
{

    protected $service;

    function __construct(OrderService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         try {
            $orders = $this->service->getAllOrders();
            if ( $orders !== null ) {
                return response()->json([
                            'data' => OrderResource::collection($orders),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Nenhum pedido cadastrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\OrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        try {
            $order = $this->service->createNewOrder($request->all());
            if ( $order !== null ) {
                return response()->json([
                            'data' => new OrderResource($order),
                            'statusCode' => 201,
                                ], 201);
            } else {
                return response()->json([
                            'message' => 'Erro ao cadastrar o pedido',
                            'statusCode' => 422
                                ], 422);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $order = $this->service->getOneOrder($id);
            if ( $order !== null ) {
                return response()->json([
                            'data' => new OrderResource($order),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Pedido não encontrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\OrderRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, $id)
    {
        try {
            $order = $this->service->updateOneOrder($request->all(), $id);
            if ( $order ) {
                return response()->json([
                            'message' => 'Pedido atualizado com sucesso.',
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Produto não encontrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->service->deleteOneOrder($id);
            if ( $data ) {
                return response()->json([
                            'data' => 'Pedido removido com sucesso.',
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Produto não encontrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

}
