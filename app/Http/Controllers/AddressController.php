<?php

namespace App\Http\Controllers;

use App\Services\AddressService;
use App\Http\Resources\AddressResource;

class AddressController extends Controller
{

    protected $service;

    function __construct(AddressService $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  string  $cep
     * @return \Illuminate\Http\Response
     */
    public function requestFullAddressFromExternApi(string $cep)
    {
        try {
            $address = $this->service->requestFullAddressFromExternApi($this->transformCep($cep));
            if ( $address !== null ) {
                return response()->json([
                            'data' => new AddressResource($address),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'data' => new AddressResource(null),
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }
    
    private function transformCep($cep)
    {
        $cepArray['cep'] = $cep;
        return $cepArray;
    }

}
