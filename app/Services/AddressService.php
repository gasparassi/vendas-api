<?php

namespace App\Services;

use App\Services\ViacepService;
use App\Services\PostmonService;

/**
 * Description of AddressService
 *
 * @author eder
 */
class AddressService
{

    function __construct()
    {
        
    }

    /**
     * Recupera o endereço válido do cliente para posterior cadastro
     * 
     * @param array $addressFromClient
     * @return array || null
     */
    public function requestFullAddressFromExternApi(array $addressFromClient)
    {
        $cep = $addressFromClient['cep'];

        $viaCepService = new ViacepService();
        $addressFromApi = $viaCepService->getFullAddress($cep, 'json');
        if ( ( $addressFromApi !== null ) && (!isset($addressFromApi['erro']) ) ) {
            return $this->compareAddressClientFromApi($addressFromApi, $addressFromClient);
        } else {
            $postManService = new PostmonService();
            $addressFromApi = $postManService->getFullAddress($cep, 'json');
            if ($addressFromApi !== null) {
                return $this->compareAddressClientFromApi($addressFromApi, $addressFromClient);
            } else {
                return null;
            } 
        }
    }

    /**
     * Faz a comparação do endereço informado pelo cliente entre o recuperado
     * pela API externa
     * 
     * @param array $addressFromApi
     * @param array $addressFromClient
     * @return array
     */
    private function compareAddressClientFromApi(array $addressFromApi, array $addressFromClient)
    {        
        if (isset($addressFromClient['numero'])){
            $numero = $addressFromClient['numero'];
            unset($addressFromClient['numero']);
        }     
        
        if (isset($addressFromClient['complemento'])){
            $complemento = $addressFromClient['complemento'];
            unset($addressFromClient['complemento']);
        }
        
        $compareAddress = array_diff($addressFromApi, $addressFromClient);

        if ( count($compareAddress) > 0 ) {
            foreach ($compareAddress as $key => $value) {
                $addressFromClient[$key] = $value;
            }
        }

        if (isset($numero)) {
            $addressFromClient['numero'] = $numero;
        } else {
            $addressFromClient['numero'] = 0;
        }
        
        if (isset($complemento)) {
            $addressFromClient['complemento'] = $complemento;
        } else {
            $addressFromClient['complemento'] = '';
        }

        return $addressFromClient;
    }

}
