<?php

namespace App\Services;

use App\Repositories\Contracts\ClientRepositoryInterface;
use App\Services\AddressService;
use App\Models\Client;

/**
 * Description of ClientService
 *
 * @author eder
 */
class ClientService
{

    private $clientRepository;

    /**
     * Construtor da classe com injeção de dependência da interface ClientRespositoryInterface
     * 
     * @param ClientRepositoryInterface $clientRepository
     */
    function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * Obtém o endereço exato que cadastrado na API externa
     * 
     * @param array $addressFromClient
     * @return array || null
     */
    private function getAddressValid(array $addressFromClient)
    {
        $addressService = new AddressService();
        return $addressService->requestFullAddressFromExternApi($addressFromClient);
    }

    /**
     * Cadastra um novo cliente no banco de dados
     * 
     * @param array $client
     * @return Client object || null
     */
    public function createNewClient(array $client)
    {
        $clientModel = new Client();

        $addressValid = $this->getAddressValid($client);

        if ( $addressValid !== null ) {
            $address = $clientModel->address()->create($addressValid);
            if ( $address !== null ) {
                $clientModel->nome = $client['nome'];
                $clientModel->endereco_id = $address->id;
                return $this->clientRepository->createClient($clientModel->toArray());
            }
        } else {
            return null;
        }
    }

    /**
     * Atualiza os dados de um cliente
     * 
     * @param array $client
     * @param int $id
     * @return boolean
     */
    public function updateOneClient(array $client, int $id)
    {
        $clientObject = $this->clientRepository->getClientByIdWithoutRelations($id);

        if ( $clientObject === null ) {
            return false;
        }

        $this->clientRepository->updateClient($clientObject, $client);

        unset($client['nome']);

        $addressValid = $this->getAddressValid($client);

        if ( $addressValid !== null ) {
            $clientObject->address()->update($addressValid);
        } else {
            return false;
        }

        return true;
    }

    /**
     * Retorna todos os clientes com seus respectivos endereços 
     * cadastrados no banco de dados
     * 
     * @return mixed || null
     */
    public function getAllClients()
    {
        $clients = $this->clientRepository->getAllClients();
        return count($clients) > 0 ? $clients : null;
    }

    /**
     * Retorna um cliente específico
     * 
     * @param int $id
     * @return Client object || null
     */
    public function getOneClient(int $id)
    {
        return $this->clientRepository->getClientByIdWithRelations($id);
    }

    /**
     * Remove um cliente específico do banco de dados
     * 
     * @param int $id
     * @return boolean
     */
    public function deleteOneClient(int $id)
    {
        $client = $this->clientRepository->getClientByIdWithoutRelations($id);

        if ( $client === null ) {
            return false;
        }

        $this->clientRepository->destroyClient($client);
        return true;
    }

}
