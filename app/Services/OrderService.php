<?php

namespace App\Services;

use App\Repositories\Contracts\OrderRepositoryInterface;

/**
 * Description of OrderService
 *
 * @author eder
 */
class OrderService
{

    protected $orderRepository;

    /**
     * Construtor da classe com injeção de dependência da interface OrderRepositoryInterface
     * 
     * @param OrderRepositoryInterface $orderRepository
     */
    function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Cadastra um novo pedido no banco de dados
     * 
     * @param array $order
     * @return Order object || null
     */
    public function createNewOrder(array $order)
    {
        $order['date_order'] = date('Y-m-d H:i:s');
        $order['products'] = json_encode($order['products']);
        return $this->orderRepository->createNewOrder($order);
    }
    
    /**
     * Retorna todos os pedidos cadastrados no banco de dados
     * 
     * @return mixed || null
     */
    public function getAllOrders()
    {
        $orders = $this->orderRepository->getAllOrders();
        return count($orders) > 0 ? $orders : null;
    }

    /**
     * Retorna um pedido específico
     * 
     * @param int $id
     * @return Order object || null
     */
    public function getOneOrder(int $id)
    {
        return $this->orderRepository->getOrderById($id);
    }

    /**
     * Atualiza os dados de um pedido
     * 
     * @param array $order
     * @param int $id
     * @return boolean
     */
    public function updateOneOrder(array $order, int $id)
    {
        $orderObject = $this->orderRepository->getOrderById($id);

        if ( $orderObject === null ) {
            return false;
        }

        $this->orderRepository->updateOrder($orderObject, $order);

        return true;
    }

    /**
     * Remove um pedido específico do banco de dados
     * 
     * @param int $id
     * @return boolean
     */
    public function deleteOneOrder(int $id)
    {
        $order = $this->orderRepository->getOrderById($id);

        if ( $order === null ) {
            return false;
        }

        $this->orderRepository->destroyOrder($order);
        return true;
    }


}
