<?php

namespace App\Services;

use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Models\Product;

/**
 * Description of ProductService
 *
 * @author eder
 */
class ProductService
{

    protected $productRepository;

    /**
     * Construtor da classe com injeção de dependência da interface ProductRepositoryInterface
     * 
     * @param ProductRepositoryInterface $productRepository
     */
    function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Cadastra um novo produto no banco de dados
     * 
     * @param array $product
     * @return Product object || null
     */
    public function createNewProduct(array $product)
    {
        return $this->productRepository->createProduct($product);
    }

    /**
     * Retorna todos os produtos cadastrados no banco de dados
     * 
     * @return mixed || null
     */
    public function getAllProducts()
    {
        $products = $this->productRepository->getAllProducts();
        return count($products) > 0 ? $products : null;
    }

    /**
     * Retorna um produto específico
     * 
     * @param int $id
     * @return Product object || null
     */
    public function getOneProduct(int $id)
    {
        return $this->productRepository->getProductById($id);
    }

    /**
     * Atualiza os dados de um produto
     * 
     * @param array $product
     * @param int $id
     * @return boolean
     */
    public function updateOneProduct(array $product, int $id)
    {
        $productObject = $this->productRepository->getProductById($id);

        if ( $productObject === null ) {
            return false;
        }

        $this->productRepository->updateProduct($productObject, $product);

        return true;
    }

    /**
     * Remove um produto específico do banco de dados
     * 
     * @param int $id
     * @return boolean
     */
    public function deleteOneProduct(int $id)
    {
        $product = $this->productRepository->getProductById($id);

        if ( $product === null ) {
            return false;
        }

        $this->productRepository->destroyProduct($product);
        return true;
    }

}
