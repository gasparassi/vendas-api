<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Contracts\{
    ClientRepositoryInterface,
    ProductRepositoryInterface,
    OrderRepositoryInterface    
};
use \App\Repositories\{
    ClientRepository,
    ProductRepository,
    OrderRepository,    
};

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
                ClientRepositoryInterface::class,
                ClientRepository::class,
        );
        
        $this->app->bind(
                ProductRepositoryInterface::class,
                ProductRepository::class,
        );
        
        $this->app->bind(
                OrderRepositoryInterface::class,
                OrderRepository::class,
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
//
    }

}
