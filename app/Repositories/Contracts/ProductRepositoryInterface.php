<?php

namespace App\Repositories\Contracts;

/**
 *
 * @author eder
 */
interface ProductRepositoryInterface
{

    public function createProduct(array $product);

    public function getProductById(int $id);

    public function updateProduct(object $productObject, array $product);

    public function getAllProducts();

    public function destroyProduct(object $product);
}
