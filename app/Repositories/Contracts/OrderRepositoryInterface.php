<?php

namespace App\Repositories\Contracts;

/**
 *
 * @author eder
 */
interface OrderRepositoryInterface
{
    public function createNewOrder(array $order);

    public function getOrderById(int $id);

    public function updateOrder(object $orderObject, array $order);

    public function getAllOrders();

    public function destroyOrder(object $order);
}
