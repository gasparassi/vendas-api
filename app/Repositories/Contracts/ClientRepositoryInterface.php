<?php

namespace App\Repositories\Contracts;

/**
 *
 * @author eder
 */
interface ClientRepositoryInterface
{

    public function createClient(array $client);

    public function getClientByIdWithRelations(int $id);
    
    public function getClientByIdWithoutRelations(int $id);

    public function updateClient(object $clientObject, array $client);

    public function getAllClients();

    public function destroyClient(object $client);
}
