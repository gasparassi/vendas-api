<?php

namespace App\Repositories;

use App\Repositories\Contracts\ClientRepositoryInterface;
use App\Models\Client;

/**
 * Description of ClientRepository
 *
 * @author eder
 */
class ClientRepository implements ClientRepositoryInterface
{

    protected $entity;

    function __construct(Client $client)
    {
        $this->entity = $client;
    }

    public function createClient(array $client)
    {
        return $this->entity->create($client);
    }

    public function destroyClient(object $client)
    {
        return $client->delete();
    }

    public function getAllClients()
    {
        return $this->entity->with('address')->paginate();
    }

    public function getClientByIdWithRelations(int $id)
    {
        return $this->entity->with('address')->where('id', '=', $id)->first();
    }

    public function getClientByIdWithoutRelations(int $id)
    {
        return $this->entity->where('id', '=', $id)->first();
    }

    public function updateClient(object $clientObject, array $client)
    {
        return $clientObject->update($client);
    }

}
