<?php

namespace App\Repositories;

use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Models\Product;

/**
 * Description of ProductRepository
 *
 * @author eder
 */
class ProductRepository implements ProductRepositoryInterface
{

    protected $entity;

    function __construct(Product $product)
    {
        $this->entity = $product;
    }

    public function createProduct(array $product)
    {
        return $this->entity->create($product);
    }

    public function destroyProduct(object $product)
    {
        return $product->delete();
    }

    public function getAllProducts()
    {
        return $this->entity->paginate();
    }

    public function getProductById(int $id)
    {
        return $this->entity->where('id', '=', $id)->first();
    }

    public function updateProduct(object $productObject, array $product)
    {
        return $productObject->update($product);
    }

}
