<?php

namespace App\Repositories;

use App\Repositories\Contracts\OrderRepositoryInterface;
use App\Models\Order;

/**
 * Description of OrderRepository
 *
 * @author eder
 */
class OrderRepository implements OrderRepositoryInterface
{

    protected $entity;

    function __construct(Order $order)
    {
        $this->entity = $order;
    }

    public function createNewOrder(array $order)
    {
        return $this->entity->create($order);
    }

    public function destroyOrder(object $order)
    {
        return $order->delete();
    }

    public function getAllOrders()
    {
        return $this->entity->paginate();
    }

    public function getOrderById(int $id)
    {
        return $this->entity->where('id', '=', $id)->first();
    }

    public function updateOrder(object $orderObject, array $order)
    {
        return $orderObject->update($order);
    }

}
